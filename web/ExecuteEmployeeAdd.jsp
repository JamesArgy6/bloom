<%-- 
    Document   : ExecuteEmployeeAdd
    Created on : Apr 29, 2015, 4:01:45 PM
    Author     : Tom
--%>

<%@page import="DBWorks.DBConnection"%>
<%@page import="java.sql.SQLException"%>
<%@ page import="java.sql.ResultSet" %>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Add Employee</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/manager.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<% 
    //Make sure that the current user has access to the page
    String login = (String)session.getAttribute("login");
    String loginQuery = "SELECT * FROM employee WHERE Role = 'Manager' AND SocialSecurityNum = " + login + ";";
    java.sql.ResultSet loginrs = DBConnection.ExecQuery(loginQuery);
    if(loginrs != null && loginrs.next())
    {
 %>  
<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="ManagerHome.jsp">Manager</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <%out.print(session.getAttribute("login")); %> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="logout.jsp"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li>
                        <a href="ManagerHome.jsp"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="Employees.jsp"><i class="fa fa-fw fa-dashboard"></i> Employees</a>
                    </li>

                    <li>
                        <a href="Customers.jsp"><i class="fa fa-fw fa-dashboard"></i> Customers</a>
                    </li>
                    <li>
                        <a href="Companys.jsp"><i class="fa fa-fw fa-dashboard"></i> Companys</a>
                    </li>
                    <li>
                        <a href="ItemTypes.jsp"><i class="fa fa-fw fa-dashboard"></i> Item Types</a>
                    </li>
                    <li>
                        <a href="AdvertisedItems.jsp"><i class="fa fa-fw fa-dashboard"></i> Advertised Items</a>
                    </li>
                    <li>
                        <a href="BestSellingItems.jsp"><i class="fa fa-fw fa-dashboard"></i> Best Selling Items</a>
                    </li>
                    <li>
                        <a href="SalesReport.jsp"><i class="fa fa-fw fa-dashboard"></i> Sales Report</a>
                    </li>
                    
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>


        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Add Employee
                        </h1>
                    </div>
                </div>
                <!-- /.row -->
                
                <div class="row">
                    <div class="col-lg-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                    Add Result
                            </div>
                            <div class="panel-body">
                                
                                
                                <% 
                                    String ssn = request.getParameter("SSN");
                                    String firstName = request.getParameter("FirstName");
                                    String lastName = request.getParameter("LastName");
                                    String address = request.getParameter("Address");
                                    String city = request.getParameter("City");
                                    String state = request.getParameter("State");
                                    String zipCode = request.getParameter("ZipCode");
                                    String telephone = request.getParameter("Telephone");
                                    String hourlyRate = request.getParameter("HourlyRate");
                                    String role = request.getParameter("Role");
                                    String password = request.getParameter("Password");
                                    
                                    String idQuery = "SELECT MAX(EmployeeId) from Employee;";
                                    java.sql.ResultSet idRS = DBConnection.ExecQuery(idQuery);
                                    idRS.next();
                                    int employeeId =  Integer.parseInt(idRS.getString(1)) + 1;
                                    
                                    String query = "INSERT INTO employee VALUES "
                                            + "('" + ssn + "', '" + lastName + "', '" + firstName + 
                                            "', '" + address + "', '" + city + "', '" + state + "', '" + zipCode + 
                                            "', '" + telephone + "', CURDATE(), '" + hourlyRate + 
                                            "', '" + role + "', '" + employeeId + "', '" + password + "');";
                                    int affectedRows = DBConnection.ExecUpdateQuery(query);
                                    if(affectedRows == 1)
                                    {
                                        out.print("Employee was added successfully!");
                                    }
                                    else
                                    {
                                        out.print("Employee was not added.");
                                    }
                                %>
                                
                                
                                <br/>
                                <a href="Employees.jsp"> Back To Employees </a>
                                
                            </div>
                        </div>
                        <!-- /.panel -->
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery Version 1.11.0 -->
    <script src="js/jquery-1.11.0.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>
<%
    }
    else
    {
        session.setAttribute("login", "");
        response.sendRedirect("index.htm");
    }
%>
</html>
