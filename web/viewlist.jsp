<%-- 
    Document   : viewlist
    Created on : May 1, 2015, 3:32:06 AM
    Author     : jodiekwok
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@page import="DBWorks.DBConnection"%>
<%@page import="java.sql.SQLException"%>
<%@ page import="java.sql.ResultSet" %>

<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Bloom</title>

        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="css/sb-admin-2.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>

        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="CRHome.jsp">Bloom-Customer-Representative</a>
                </div>
                <!-- /.navbar-header -->

                <ul class="nav navbar-top-links navbar-right">
                    <!-- /.dropdown -->
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="CRHelp.jsp"><i class="fa fa-question fa-fw"></i> Help</a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="logout.jsp"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                            </li>
                        </ul>
                        <!-- /.dropdown-user -->
                    </li>
                    <!-- /.dropdown -->
                </ul>
                <!-- /.navbar-top-links -->

                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <li>
                        <a href="CreateAd.jsp"><i class="fa fa-file-text-o fa-fw"></i> Create an advertisement</a>
                    </li>
                    <li>
                        <a href="DeleteAd.jsp"><i class="fa fa-trash-o fa-fw"></i> Delete an advertisement</a>
                    </li>
                    <li>
                        <a href="ViewTransaction.jsp"><i class="fa fa-fw fa-pencil"></i> Record a transaction</a>
                    </li>
                    <li>
                        <a href="EditCustomerInfo.jsp"><i class="fa fa-edit fa-fw"></i> Edit customer information</a>
                    </li>
                    <li>
                        <a href="MailingList.jsp"><i class="fa fa-fw fa-list"></i> Customer mailing list</a>
                    </li>
                    <li>
                        <a href="SuggestList.jsp"><i class="fa fa-fw fa-check-square-o"></i> Item suggestions list</a>
                    </li>
                            

                        </ul>
                    </div>
                    <!-- /.sidebar-collapse -->
                </div>
                <!-- /.navbar-static-side -->
            </nav>

            <!-- Page Content -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Item Suggestions List</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-warning">
                            <div class="panel-heading">
                                <i class="fa fa-check-square-o fa-fw"></i> Item Suggestions List
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <%
                                            int customerid = Integer.parseInt(request.getParameter("customerid"));                             
                                            String query = "SELECT * FROM customer WHERE CustomerId = '"
                                                    + customerid + "'";
                                            java.sql.ResultSet rs = DBConnection.ExecQuery(query);
                                            if (rs.next()) {
                                        %>
                                        <p> <strong>Customer ID: </strong> <%= customerid %></p>
                                        <% } %>
                                    </div>
                                </div>
                                <table class="table table-striped">
                                    <thead>
                                        <tr>                                            
                                            <th>Item Name</th>
                                            <th>Advertisement ID</th>
                                        </tr>
                                    </thead>
                                    <tbody> 
                                        <%
                                            Integer employeeid = (Integer) session.getAttribute("ID");
                                            String getlist
                                                        = "SELECT DISTINCT adv1.ItemName, adv1.AdvertisementID FROM sale s"
                                                        + " JOIN account a ON s.AccountNum = a.AccountNum"
                                                        + " AND a.CustomerId = '" + customerid + "'"
                                                        + " JOIN advertisement adv ON s.AdvtId = adv.AdvertisementID"
                                                        + " JOIN advertisement adv1 ON adv1.Type = adv.Type AND"
                                                        + " adv1.NumberOfAvailableUnits > 0 AND"
                                                        + " adv1.EmployeeId = '" + employeeid + "'";
                                            java.sql.ResultSet suggestitems = DBConnection.ExecQuery(getlist);
                                            while (suggestitems.next()) {
                                        %>
                                        <tr>
                                            <td><%= suggestitems.getString("ItemName")%></td>
                                            <td><%= suggestitems.getString("AdvertisementID")%></td>                                                                                     
                                        </tr>
                                    <%
                                        }
                                    %>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->

        <!-- jQuery Version 1.11.0 -->
        <script src="js/jquery-1.11.0.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="js/plugins/metisMenu/metisMenu.min.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="js/sb-admin-2.js"></script>

    </body>

</html>