<%-- 
    Document   : newInfo
    Created on : May 1, 2015, 1:20:55 AM
    Author     : jodiekwok
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%@page import="DBWorks.DBConnection"%>
<%@page import="java.sql.SQLException"%>
<%@ page import="java.sql.ResultSet" %>

<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Bloom</title>

        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="css/sb-admin-2.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>

        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="CRHome.jsp">Bloom-Customer-Representative</a>
                </div>
                <!-- /.navbar-header -->

                <ul class="nav navbar-top-links navbar-right">
                    <!-- /.dropdown -->
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="CRHelp.jsp"><i class="fa fa-question fa-fw"></i> Help</a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="logout.jsp"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                            </li>
                        </ul>
                        <!-- /.dropdown-user -->
                    </li>
                    <!-- /.dropdown -->
                </ul>
                <!-- /.navbar-top-links -->

                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <li>
                        <a href="CreateAd.jsp"><i class="fa fa-file-text-o fa-fw"></i> Create an advertisement</a>
                    </li>
                    <li>
                        <a href="DeleteAd.jsp"><i class="fa fa-trash-o fa-fw"></i> Delete an advertisement</a>
                    </li>
                    <li>
                        <a href="ViewTransaction.jsp"><i class="fa fa-fw fa-pencil"></i> Record a transaction</a>
                    </li>
                    <li>
                        <a href="EditCustomerInfo.jsp"><i class="fa fa-edit fa-fw"></i> Edit customer information</a>
                    </li>
                    <li>
                        <a href="MailingList.jsp"><i class="fa fa-fw fa-list"></i> Customer mailing list</a>
                    </li>
                    <li>
                        <a href="SuggestList.jsp"><i class="fa fa-fw fa-check-square-o"></i> Item suggestions list</a>
                    </li>
                            

                        </ul>
                    </div>
                    <!-- /.sidebar-collapse -->
                </div>
                <!-- /.navbar-static-side -->
            </nav>

            <!-- Page Content -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Edit customer information</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-success">
                            <div class="panel-heading">Customer information updated!</div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-warning">
                            <div class="panel-heading">
                                <i class="fa fa-inbox fa-fw"></i> Customer information
                            </div>
                            <div class="panel-body">
                                <%
                                    int customerid = Integer.parseInt(request.getParameter("customerid"));
                                    String newlastn = request.getParameter("newlastname");
                                    String newfn = request.getParameter("newfirstname");
                                    String newadd = request.getParameter("newaddress");
                                    String newcity = request.getParameter("newcity");
                                    String newstate = request.getParameter("newstate");
                                    String newzip = request.getParameter("newzip");
                                    String newemail = request.getParameter("newemail");
                                    String newdob = request.getParameter("newdob");
                                    String newsex = request.getParameter("newsex");
                                    String newtel = request.getParameter("newtel");
                                    String newrate = request.getParameter("newrating");
                                    if (newlastn.equals("")) {
                                %>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="panel panel-danger">
                                            <div class="panel-heading">Customer's last name cannot be empty!</div>
                                        </div>
                                    </div>
                                </div>
                                <%
                                    } else {
                                        String editln = "UPDATE customer SET LastName = '"
                                                + newlastn + "' WHERE CustomerId = '" + customerid + "'";
                                        DBConnection.ExecUpdateQuery(editln);
                                    }
                                    if (newfn.equals("")) {
                                %>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="panel panel-danger">
                                            <div class="panel-heading">Customer's first name cannot be empty!</div>
                                        </div>
                                    </div>
                                </div>
                                <%
                                    } else {
                                        String editfn = "UPDATE customer SET FirstName = '"
                                                + newfn + "' WHERE CustomerId = '" + customerid + "'";
                                        DBConnection.ExecUpdateQuery(editfn);
                                    }
                                    if (newadd.equals("") || newadd.equals("null")) {
                                        String deladdress = "UPDATE customer SET Address = null"
                                                + " WHERE CustomerId = '" + customerid + "'";
                                        DBConnection.ExecUpdateQuery(deladdress);
                                    } else {
                                        String editaddress = "UPDATE customer SET Address = '"
                                                + newadd + "' WHERE CustomerId = '" + customerid + "'";
                                        DBConnection.ExecUpdateQuery(editaddress);
                                    }
                                    if (newcity.equals("") || newcity.equals("null")) {
                                        String delcity = "UPDATE customer SET City = null"
                                                + " WHERE CustomerId = '" + customerid + "'";
                                        DBConnection.ExecUpdateQuery(delcity);
                                    } else {
                                        String editcity = "UPDATE customer SET City = '"
                                                + newcity + "' WHERE CustomerId = '" + customerid + "'";
                                        DBConnection.ExecUpdateQuery(editcity);
                                    }
                                    if (newstate.equals("")) {
                                        String delstate = "UPDATE customer SET State = null"
                                                + " WHERE CustomerId = '" + customerid + "'";
                                        DBConnection.ExecUpdateQuery(delstate);
                                    } else {
                                        String editstate = "UPDATE customer SET State = '"
                                                + newstate + "' WHERE CustomerId = '" + customerid + "'";
                                        DBConnection.ExecUpdateQuery(editstate);
                                    }
                                    if (newzip.equals("")) {
                                        String delzip = "UPDATE customer SET ZipCode = null"
                                                + " WHERE CustomerId = '" + customerid + "'";
                                        DBConnection.ExecUpdateQuery(delzip);
                                    } else {
                                        String editzip = "UPDATE customer SET ZipCode = '"
                                                + newzip + "' WHERE CustomerId = '" + customerid + "'";
                                        DBConnection.ExecUpdateQuery(editzip);
                                    }
                                    if (newemail.equals("")) {
                                %>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="panel panel-danger">
                                            <div class="panel-heading">Customer's email cannot be empty!</div>
                                        </div>
                                    </div>
                                </div>
                                <%
                                    } else {
                                        String editfn = "UPDATE customer SET Email = '"
                                                + newemail + "' WHERE CustomerId = '" + customerid + "'";
                                        DBConnection.ExecUpdateQuery(editfn);
                                    }
                                    if (newdob.equals("")) {
                                        String deldob = "UPDATE customer SET DOB = null"
                                                + " WHERE CustomerId = '" + customerid + "'";
                                        DBConnection.ExecUpdateQuery(deldob);
                                    } else {
                                        String editdob = "UPDATE customer SET DOB = '"
                                                + newdob + "' WHERE CustomerId = '" + customerid + "'";
                                        DBConnection.ExecUpdateQuery(editdob);
                                    }
                                    if (newsex.equals("")) {
                                        String delsex = "UPDATE customer SET Sex = null"
                                                + " WHERE CustomerId = '" + customerid + "'";
                                        DBConnection.ExecUpdateQuery(delsex);
                                    } 
                                    else {
                                        String editsex = "UPDATE customer SET Sex = '"
                                                + newsex + "' WHERE CustomerId = '" + customerid + "'";
                                        DBConnection.ExecUpdateQuery(editsex);
                                    }
                                    if (newtel.equals("")) {
                                        String deltel = "UPDATE customer SET Telephone = null"
                                                + " WHERE CustomerId = '" + customerid + "'";
                                        DBConnection.ExecUpdateQuery(deltel);
                                    } else {
                                        String edittel = "UPDATE customer SET Telephone = '"
                                                + newtel + "' WHERE CustomerId = '" + customerid + "'";
                                        DBConnection.ExecUpdateQuery(edittel);
                                    }
                                    if (newrate.equals("")) {
                                        String delrate = "UPDATE customer SET Rating = null"
                                                + " WHERE CustomerId = '" + customerid + "'";
                                        DBConnection.ExecUpdateQuery(delrate);
                                    } else {
                                        String editrate = "UPDATE customer SET Rating = '"
                                                + newrate + "' WHERE CustomerId = '" + customerid + "'";
                                        DBConnection.ExecUpdateQuery(editrate);
                                    }
                                    String query = "SELECT * FROM customer WHERE CustomerId = '"
                                            + customerid + "'";
                                    java.sql.ResultSet rs = DBConnection.ExecQuery(query);
                                    if (rs.next()) {
                                %>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <p> <strong>Customer ID: </strong> <%= customerid%></p>
                                        <p> <strong>Last Name: </strong> <%= rs.getString("LastName")%></p>
                                        <p> <strong>First Name: </strong> <%= rs.getString("FirstName")%></p>
                                        <p> <strong>Address: </strong> <%= rs.getString("Address")%></p>
                                        <p> <strong>City: </strong> <%= rs.getString("City")%></p>
                                        <p> <strong>State: </strong> <%= rs.getString("State")%></p>
                                        <p> <strong>Zip Code: </strong> <%= rs.getString("ZipCode")%></p>
                                    </div>
                                    <div>
                                        <p> <strong>Email: </strong> <%= rs.getString("Email") %></p>
                                        <p> <strong>Date of Birth: </strong> <%= rs.getString("DOB") %></p>
                                        <p> <strong>Sex: </strong> <%= rs.getString("Sex") %></p>
                                        <p> <strong>Telephone: </strong> <%= rs.getString("Telephone") %></p>
                                        <p> <strong>Creation Date: </strong> <%= rs.getString("CreationDate") %></p>
                                        <p> <strong>Rating: </strong> <%= rs.getString("Rating") %></p>
                                    </div>
                                    <% } %>
                                    <!-- /.col-lg-6 (nested) -->
                                </div>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->

        <!-- jQuery Version 1.11.0 -->
        <script src="js/jquery-1.11.0.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="js/plugins/metisMenu/metisMenu.min.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="js/sb-admin-2.js"></script>

    </body>

</html>
