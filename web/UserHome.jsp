<%-- 
    Document   : newjsp
    Created on : Apr 28, 2015, 10:10:01 AM
    Author     : James
--%>
<%@page import="DBWorks.DBConnection"%>
<%@page import="java.sql.SQLException"%>
<%@ page import="java.sql.ResultSet" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Bloom - Home</title>

        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/customuserhome.css" rel="stylesheet">
        <link href="css/customusercss.css" rel="stylesheet">
        <!-- Custom CSS -->
        <!--            <link href="css/sb-admin.css" rel="stylesheet">-->
        <!--            <link href="css/customuserhome.css" rel="stylesheet">-->

        <!-- Custom Fonts -->
        <link href="font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>

        <%
            String userID = session.getAttribute("ID").toString();
            String query = "SELECT FirstName FROM customer WHERE customer.customerid = '" + userID + "'";
            java.sql.ResultSet rs = DBConnection.ExecQuery(query);
            String userName = "Bloomer";
            if (rs.next())
            {
                userName = rs.getString("FirstName");
            }
        %>
        <header class="navbar navbar-default navbar-static-top" style="height:50px;" role="banner">
            <div class="container" style="margin-left:0px;padding-left:0px">
                <div class="navbar-header">
                    <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="/Bloom/UserHome.jsp" class="navbar-brand" style="padding:0px;margin-left:0px"><img src="img/Icon.png" alt="Bloom"></a>
                </div>
                <nav class="collapse navbar-collapse" role="navigation">
                    <ul class="nav navbar-nav">
                        <li><a style="color:#00FF7F">Hello, <%out.print(userName);%>!</a></li>
                        <li><a href="logout.jsp">Log Out</a></li>
                    </ul>
                    <form class="navbar-form" role="search">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for a User or Circle">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="Submit">Go!</button>
                                    </span>
                                </div><!-- /input-group -->
                            </div><!-- /.col-lg-6 -->
                        </div><!-- /.row -->
                    </form>
                </nav>
            </div>
        </header>

        <!-- Begin Body -->
        <div class="container" style="margin-left:10px">
            <div class="row">
                <div class="col-md-3" id="leftCol">
                    <div class="well"> 
                        <ul class="nav nav-stacked" id="sidebar">
                            <li><a href="#sec1">Section 1</a></li>
                            <li><a href="#sec2">Section 2</a></li>
                            <li><a href="#sec3">Section 3</a></li>
                            <li><a href="#sec4">Section 4</a></li>
                        </ul>
                    </div>
                </div>  



                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading"><h3>My Circles</h3></div>
                        <div class="container" style="width:inherit">
                            <%
                                userID = session.getAttribute("ID").toString();
                                query = "SELECT * from Circle WHERE Circle.Owner = '" + userID + "'";
                                rs = DBConnection.ExecQuery(query);
                                int i = 0;
                                while (rs.next())
                                {
                                    String circleName = rs.getString("CircleName");
                                    String circleType = rs.getString("Type");
                                    String circleID = rs.getString("CircleId");
                                    i++;
                            %>
                            <a style="cursor:pointer" onclick="javascript:

                                            window.open('Circle.jsp?circleid=<%=circleID%>', '_self');

                                    return;">
                                <h2 class="circlename"><%out.print(circleName);%><a href="EditCircle.jsp"><span class="glyphicon glyphicon-pencil" style="float:right"></span></a></h2>
                            </a>    
                            <h3 style="margin-top:3px">&nbsp;&nbsp;&nbsp;&nbsp;<%out.print(circleType);%></h3>
                            <%
                                }
                                if (i == 0)
                                {
                            %>
                            <p>You do not own any circles.</p>
                            <%
                                }
                            %>
                        </div>

                    </div>
                </div>


                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading"><h3>Circles I'm a Part of</h3></div>
                        <div class="container" style="width:inherit">
                            <%
                                query = "SELECT * from Circle WHERE Circle.CircleId in (SELECT CircleID FROM joinedcircle WHERE joinedcircle.customerid = " + userID + ")";
                                rs = DBConnection.ExecQuery(query);
                                i = 0;
                                while (rs.next())
                                {
                                    String circleName = rs.getString("CircleName");
                                    String circleType = rs.getString("Type");
                                    String circleID = rs.getString("CircleId");
                                    i++;
                            %>
                            <a style="cursor:pointer" onclick="javascript:

                                            window.open('Circle.jsp?circleid=<%=circleID%>', '_self');

                                    return;">
                                <h2 class="circlename"><%out.print(circleName);%></h2>
                            </a>
                            <h3 style="margin-top:3px">&nbsp;&nbsp;&nbsp;&nbsp;<%out.print(circleType);%></h3>
                            <hr>
                            <%
                                }
                            %>

                        </div>

                    </div>
                </div>

            </div> 
        </div>

    </body>
</html>
