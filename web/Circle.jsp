<%-- 
    Document   : newjsp
    Created on : Apr 28, 2015, 10:10:01 AM
    Author     : James
--%>
<%@page import="DBWorks.DBConnection"%>
<%@page import="java.sql.SQLException"%>
<%@ page import="java.sql.ResultSet" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Bloom - Circle</title>

        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/customcircle.css" rel="stylesheet">
        <link href="css/customusercss.css" rel="stylesheet">
        <!-- Custom CSS -->
        <!--            <link href="css/sb-admin.css" rel="stylesheet">-->
        <!--            <link href="css/customuserhome.css" rel="stylesheet">-->

        <!-- Custom Fonts -->
        <link href="font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>

        <%
            String circleID = request.getParameter("circleid");
            String userID = session.getAttribute("ID").toString();
            String query = "SELECT FirstName FROM customer WHERE customer.customerid = '" + userID + "'";
            java.sql.ResultSet rs = DBConnection.ExecQuery(query);
            String userName = "Bloomer";
            if (rs.next())
            {
                userName = rs.getString("FirstName");
            }
        %>
        <header class="navbar navbar-default navbar-static-top" style="height:50px;" role="banner">
            <div class="container" style="margin-left:0px;padding-left:0px">
                <div class="navbar-header">
                    <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="/Bloom/UserHome.jsp" class="navbar-brand" style="padding:0px;margin-left:0px"><img src="img/Icon.png" alt="Bloom"></a>
                </div>
                <nav class="collapse navbar-collapse" role="navigation">
                    <ul class="nav navbar-nav">
                        <li><a style="color:#00FF7F">Hello, <%out.print(userName);%>!</a></li>
                        <li><a href="logout.jsp">Log Out</a></li>
                    </ul>
                    <form class="navbar-form" role="search">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for a User or Circle">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="Submit">Go!</button>
                                    </span>
                                </div><!-- /input-group -->
                            </div><!-- /.col-lg-6 -->
                        </div><!-- /.row -->
                    </form>
                </nav>
            </div>
        </header>

        <!-- Begin Body -->
        <div class="container" style="margin-left:10px">
            <div class="row">
                <div class="col-md-3" id="leftCol">
                    <div class="well"> 
                        <ul class="nav nav-stacked" id="sidebar">
                            <li><a href="#sec1">Section 1</a></li>
                            <li><a href="#sec2">Section 2</a></li>
                            <li><a href="#sec3">Section 3</a></li>
                            <li><a href="#sec4">Section 4</a></li>
                        </ul>
                    </div>
                </div>  

                <div class="col-md-9">
                    <div class="panel panel-default">
                        <div class="panel-heading"> <%
                            query = "SELECT * FROM circle WHERE circleid = " + circleID;
                            rs = DBConnection.ExecQuery(query);
                            String circleName = "null";
                            String circleType = "null";
                            String circleOwner = "null";
                            if (rs.next())
                            {
                                circleName = rs.getString("CircleName");
                                circleType = rs.getString("Type");
                                circleOwner = rs.getString("Owner");
                            }
                            query = "SELECT * FROM page WHERE page.AssociatedCircle =" + circleID;
                            rs = DBConnection.ExecQuery(query);
                            String postCount = "null";
                            if (rs.next())
                            {
                                postCount = rs.getString("PostCount");
                            }
                            query = "SELECT FirstName, LastName FROM customer WHERE customerid = " + circleOwner;
                            rs = DBConnection.ExecQuery(query);
                            String ownerFirst = "null";
                            String ownerLast = "null";
                            if (rs.next())
                            {
                                ownerFirst = rs.getString("FirstName");
                                ownerLast = rs.getString("LastName");
                            }
                            %>
                            <h2 style="margin-bottom:-10px"><%out.print(circleName);%></h2>
                            <hr style="color:#ddd">
                            <h3>&nbsp;&nbsp;&nbsp;&nbsp;Circle owned by <%out.print(ownerFirst + " " + ownerLast);%></h3> 
                        </div>
                        <div class="container" style="width:inherit;padding:20px">
                            <div class="panel panel-default post-panel">
                                <div class="panel-heading post-header" style="height:150px">
                                    <form action="post.jsp?&circleID=<%=circleID%>" method="post">
                                        <textarea name="post_text" cols="100" rows="3" style="float:left"></textarea>                                    
                                        <button class="btn btn-primary btn-lg btn-custom btn-block" style="float:left;width:200px;margin-top:10px">Make Post</button>
                                    </form>                     
                                </div>
                            </div>
                            <%
                                query = "SELECT * from post WHERE post.PostID IN (SELECT postedon.PostID from postedon WHERE postedon.PageID = (SELECT page.PageID FROM page WHERE page.AssociatedCircle =" + circleID + ")) ORDER BY post.PostID DESC";
                                rs = DBConnection.ExecQuery(query);
                                String postID = "null";
                                String content = "null";
                                String postDate = "null";
                                String postedBy = "null";
                                String commentCount = "null";
                                rs = DBConnection.ExecQuery(query);
                                while (rs.next())
                                {
                                    content = rs.getString("Content");
                                    postDate = rs.getString("PostDate");
                                    postID = rs.getString("PostID");
                                    postedBy = rs.getString("Author");
                                    String postedByID = postedBy;
                                    commentCount = rs.getString("CommentCount");
                                    query = "SELECT FirstName, LastName FROM customer WHERE customerid = " + postedBy;
                                    java.sql.ResultSet rs2 = DBConnection.ExecQuery(query);
                                    while (rs2.next())
                                    {
                                        postedBy = rs2.getString("FirstName") + " " + rs2.getString("LastName");
                                    }
                                    query = "SELECT COUNT(*) FROM postlike WHERE postID = " + postID;
                                    java.sql.ResultSet rs5 = DBConnection.ExecQuery(query);
                                    String postLikes = "0";
                                    if (rs5.next())
                                    {
                                        postLikes = rs5.getString(1);
                                    }
                                    query = "SELECT LikedBy FROM postlike WHERE PostID = " + postID;
                                    rs5 = DBConnection.ExecQuery(query);
                                    String badgeColor = "#000";
                                    String isLiked = "false";
                                    while (rs5.next())
                                    {
                                        if (rs5.getString("LikedBy").equals(userID))
                                        {
                                            badgeColor = "#00CC66";
                                            isLiked = "true";
                                        }
                                    }
                            %>
                            <div class="panel panel-default post-panel">
                                <div class="panel-heading post-header"> 
                                    <h4>Post by <%out.print(postedBy);%> on <%out.print(postDate + ":");%></h4>
                                    <%if (postedByID.equals(session.getAttribute("ID").toString()))
                                        {%>
                                    <a  style="float:right; margin-top:-35px; margin-right:30px;color:black" onclick="javascript:
                                                    var newPost = prompt('Edit your post:', '<%=content%>');
                                            if (newPost !== null)
                                                window.open('edit.jsp?media=post&ID=<%=postID%>&circleID=<%=circleID%>&newdata=' + newPost, '_self');

                                            return;"><span class="glyphicon glyphicon-pencil" style="font-size:16px;"></span></a>
                                        <%}%>
                                        <%
                                            if (postedByID.equals(session.getAttribute("ID").toString()) || circleOwner.equals(session.getAttribute("ID").toString()))
                                            {%>
                                    <a  style="float:right; margin-top:-35px;color:black" href="delete.jsp?media=post&ID=<%=postID%>&circleID=<%=circleID%>"><span class="glyphicon glyphicon-remove" style="font-size:20px;"></span></a>
                                        <%}%>
                                    <p class="post">&nbsp;&nbsp;&nbsp;&nbsp;<%out.print(content);%></p>
                                    <span role="presentation" class="active" style="float:right;margin-top:-35px"><a href="like.jsp?media=post&ID=<%=postID%>&liked=<%=isLiked%>&circleID=<%=circleID%>"><span class="glyphicon glyphicon-thumbs-up" style="font-size:40px;"></span></a><span class="badge" style="background-color:<%out.print(badgeColor);%>"><%out.print(postLikes);%></span></span>
                                </div>
                                <div class="container comments" style="width:inherit">
                                    <%
                                        query = "SELECT * from comment WHERE CommentID IN (SELECT CommentID FROM commentedon WHERE PostID = " + postID + ") ORDER BY CommentID ";
                                        java.sql.ResultSet rs3 = DBConnection.ExecQuery(query);
                                        String commentID = "null";
                                        String commentDate = "null";
                                        String commentContent = "null";
                                        String commentAuthor = "null";
                                        while (rs3.next())
                                        {
                                            commentID = rs3.getString("CommentID");
                                            commentDate = rs3.getString("CommentDate");
                                            commentContent = rs3.getString("Content");
                                            commentAuthor = rs3.getString("Author");
                                            String authorName = "null";
                                            query = "SELECT FirstName, LastName FROM customer WHERE customerid = " + commentAuthor;
                                            java.sql.ResultSet rs4 = DBConnection.ExecQuery(query);
                                            if (rs4.next())
                                            {
                                                authorName = rs4.getString("FirstName") + " " + rs4.getString("LastName");
                                            }
                                            query = "SELECT COUNT(*) FROM commentlike WHERE commentID = " + commentID;
                                            rs5 = DBConnection.ExecQuery(query);
                                            String commentLikes = "0";
                                            if (rs5.next())
                                            {
                                                commentLikes = rs5.getString(1);
                                            }
                                            query = "SELECT LikedBy FROM commentlike WHERE CommentID = " + commentID;
                                            rs5 = DBConnection.ExecQuery(query);
                                            badgeColor = "#000";
                                            isLiked = "false";
                                            while (rs5.next())
                                            {
                                                if (rs5.getString("LikedBy").equals(userID))
                                                {
                                                    badgeColor = "#00CC66";
                                                    isLiked = "true";
                                                }
                                            }

                                    %>
                                    <h5><%out.print(authorName);%> on <%out.print(postDate + ":");%></h5>
                                    <%if (commentAuthor.equals(session.getAttribute("ID").toString()))
                                        {%>
                                    <a  style="float:right; margin-top:-25px; margin-right:20px;color:black" onclick="javascript:
                                                    var newComment = prompt('Edit your comment:', '<%=commentContent%>');
                                            if (newComment !== null)
                                                window.open('edit.jsp?media=comment&ID=<%=commentID%>&circleID=<%=circleID%>&newdata=' + newComment, '_self');

                                            return;"><span class="glyphicon glyphicon-pencil" style="font-size:16px;"></span></a>
                                        <%}%>
                                        <%if (commentAuthor.equals(session.getAttribute("ID").toString()) || circleOwner.equals(session.getAttribute("ID").toString()))
                                            {%>
                                    <a  style="float:right; margin-top:-25px;color:black" href="delete.jsp?media=comment&ID=<%=commentID%>&circleID=<%=circleID%>"><span class="glyphicon glyphicon-remove" style="font-size:16px;"></span></a>
                                        <%}%>
                                    <p class="post"><br>&nbsp;&nbsp;&nbsp;&nbsp;<%out.print(commentContent);%></p>
                                    <span role="presentation" class="active" style="float:right;margin-top:-35px"><a href="like.jsp?media=comment&ID=<%=commentID%>&liked=<%=isLiked%>&circleID=<%=circleID%>"><span class="glyphicon glyphicon-thumbs-up" style="font-size:30px;"></span></a><span class="badge" style="background-color:<%out.print(badgeColor);%>"><%out.print(commentLikes);%></span></span>
                                    <hr style="border-top: 1px solid #787B79;">
                                    <%
                                        }
                                    %>
                                </div>
                                <div class="panel-footer post-footer">
                                    <form action="comment.jsp?postID=<%out.print(postID);%>&circleID=<%=circleID%>" method="post">
                                        <textarea name="comment_text" cols="100" rows="3" style="float:left"></textarea>                                    
                                        <button class="btn btn-primary btn-lg btn-custom btn-block" style="float:left;width:200px;margin-top:10px">Comment</button>
                                    </form>
                                    <span role="presentation" class="active" style="float:right;font-size:18px;margin-top:20px">Total Comments <span class="badge"><%out.print(commentCount);%></span></span>
                                </div>
                            </div>
                            <% }
                            %>
                            <span role="presentation" class="active" style="float:right;font-size:24px">Total Posts <span class="badge"><%out.print(postCount);%></span></span>
                        </div>

                    </div>
                </div>

            </div> 
        </div>

    </body>
</html>

