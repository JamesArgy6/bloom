<%-- 
    Document   : addcustomer
    Created on : May 6, 2015, 10:10:53 PM
    Author     : jodiekwok
--%>

<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%@page import="DBWorks.DBConnection"%>
<%@page import="java.sql.SQLException"%>
<%@ page import="java.sql.ResultSet" %>

<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Bloom</title>

        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="css/sb-admin-2.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>

        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="CRHome.jsp">Bloom-Customer-Representative</a>
                </div>
                <!-- /.navbar-header -->

                <ul class="nav navbar-top-links navbar-right">
                    <!-- /.dropdown -->
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="CRHelp.jsp"><i class="fa fa-question fa-fw"></i> Help</a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="logout.jsp"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                            </li>
                        </ul>
                        <!-- /.dropdown-user -->
                    </li>
                    <!-- /.dropdown -->
                </ul>
                <!-- /.navbar-top-links -->

                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <li>
                        <a href="CreateAd.jsp"><i class="fa fa-file-text-o fa-fw"></i> Create an advertisement</a>
                    </li>
                    <li>
                        <a href="DeleteAd.jsp"><i class="fa fa-trash-o fa-fw"></i> Delete an advertisement</a>
                    </li>
                    <li>
                        <a href="ViewTransaction.jsp"><i class="fa fa-fw fa-pencil"></i> Record a transaction</a>
                    </li>
                    <li>
                        <a href="EditCustomerInfo.jsp"><i class="fa fa-edit fa-fw"></i> Edit customer information</a>
                    </li>
                    <li>
                        <a href="MailingList.jsp"><i class="fa fa-fw fa-list"></i> Customer mailing list</a>
                    </li>
                    <li>
                        <a href="SuggestList.jsp"><i class="fa fa-fw fa-check-square-o"></i> Item suggestions list</a>
                    </li>
                            

                        </ul>
                    </div>
                    <!-- /.sidebar-collapse -->
                </div>
                <!-- /.navbar-static-side -->
            </nav>

            <!-- Page Content -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">New customer information</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                
                <%
                    String cid = "SELECT MAX(CustomerId) FROM customer";
                    int customerid = 0;
                    java.sql.ResultSet rscid = DBConnection.ExecQuery(cid);
                    if (rscid.next()) {
                        customerid = rscid.getInt("MAX(CustomerId)") + 1;
                        }
                    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
                    Date date = new Date();
                    String creationdate = dateFormat.format(date);
                %> 
                
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-danger">
                            <div class="panel-heading">
                                <i class="fa fa-edit fa-fw"></i> New customer information
                            </div>
                            <div class="panel-body">
                                <form role="form" method="get" action="newCustomer.jsp">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="panel panel-info">
                                                <div class="panel-heading">Please enter all the information!</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <input type="hidden" name="customerid" value="<%= customerid%>" > 
                                            <div class="form-group">
                                                <lable>Last Name</lable>
                                                <input type="text" class="form-control" name="newlastname" placeholder="Last Name">
                                            </div>
                                            <div class="form-group">
                                                <lable>First Name</lable>
                                                <input type="text" class="form-control" name="newfirstname" placeholder="First Name">                                                   
                                            </div>
                                            <div class="form-group">
                                                <lable>Address</lable>
                                                <input class="form-control" name="newaddress" placeholder="Address">                                                 
                                            </div> 
                                            <div class="form-group">
                                                <label>City</label>
                                                <input type="text" class="form-control" name="newcity" placeholder="City">
                                            </div>
                                            <div class="form-group">
                                                <label>State</label>
                                                <input type="text" class="form-control" name="newstate" placeholder="State">
                                            </div>
                                            <div class="form-group">
                                                <label>Zip Code</label>
                                                <input type="text" class="form-control" name="newzip" placeholder="Zip Code">                                                
                                            </div>
                                            <!-- </form>-->
                                        </div>
                                        <!-- /.col-lg-6 (nested) -->
                                        <div class="col-lg-6">
                                            <!--<form role="form">-->
                                            <div class="form-group">
                                                <label>Email</label>
                                                <input type="text" class="form-control" name="newemail" placeholder="Email">
                                            </div>
                                            <div class="form-group">
                                                <label>Date of Birth (yyyy/MM/dd)</label>
                                                <input type="text" class="form-control" name="newdob" placeholder="yyyy/MM/dd">
                                            </div>
                                            <div class="form-group">
                                                <label>Sex (F/M)</label>
                                                <input type="tel" class="form-control" name="newsex" placeholder="(F/M)">
                                            </div> 
                                            <div class="form-group">
                                                <label>Telephone</label>
                                                <input type="tel" class="form-control" name="newtel" placeholder="Telephone">
                                            </div>
                                            <div class="form-group">
                                                <label>Creation Date</label>
                                                <input type="hidden" name="creationdate" value="<%= creationdate%>" >
                                                <p> <%= creationdate%></p>
                                            </div>

                                            <div class="form-group">
                                                <label>Rating</label>
                                                <input type="text" class="form-control" name="newrating" placeholder="Rating">
                                            </div>                                           
                                        </div>
                                        <!-- /.col-lg-6 (nested) -->
                                    </div>
                                    <!-- /.row (nested) -->
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <!--<form role="form">-->
                                            <button type="submit" class="btn btn-outline btn-primary" vlaue="add">Submit</button>
                                            <button type="reset" class="btn btn-outline btn-danger">Reset</button>
                                            <!-- </form>-->
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->

        <!-- jQuery Version 1.11.0 -->
        <script src="js/jquery-1.11.0.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="js/plugins/metisMenu/metisMenu.min.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="js/sb-admin-2.js"></script>

    </body>

</html>
