<%-- 
    Document   : like
    Created on : May 2, 2015, 1:23:38 AM
    Author     : James
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="DBWorks.DBConnection"%>
<%@page import="java.sql.SQLException"%>
<%@ page import="java.sql.ResultSet" %>
<%
        if (request.getParameter("media").equals("post"))
        {
            String isLiked = request.getParameter("liked");
            String postID = request.getParameter("ID");
            String userID = session.getAttribute("ID").toString();
            String circleID = request.getParameter("circleID");
            String query;
            if (isLiked.equals("false"))
            {
                query = "INSERT INTO postlike VALUES (" + postID + ", " + userID + ")";
                DBConnection.ExecUpdateQuery(query);
                response.sendRedirect("Circle.jsp?circleid=" + circleID);
            }
            else
            {
                query = "DELETE FROM postlike WHERE PostID = " + postID + " AND LikedBy = " + userID;
                DBConnection.ExecUpdateQuery(query);
                response.sendRedirect("Circle.jsp?circleid=" + circleID);
            }
        }
        else if (request.getParameter("media").equals("comment"))
        {
            String isLiked = request.getParameter("liked");
            String commentID = request.getParameter("ID");
            String userID = session.getAttribute("ID").toString();
            String circleID = request.getParameter("circleID");
            String query;
            if (isLiked.equals("false"))
            {
                query = "INSERT INTO commentlike VALUES (" + commentID + ", " + userID + ")";
                DBConnection.ExecUpdateQuery(query);
                response.sendRedirect("Circle.jsp?circleid=" + circleID);
            }
            else
            {
                query = "DELETE FROM commentlike WHERE CommentID = " + commentID + " AND LikedBy = " + userID;
                DBConnection.ExecUpdateQuery(query);
                response.sendRedirect("Circle.jsp?circleid=" + circleID);
            }
        }
    
%>