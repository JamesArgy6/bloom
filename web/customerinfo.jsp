<%-- 
    Document   : customerinfo
    Created on : May 1, 2015, 12:00:21 AM
    Author     : jodiekwok
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%@page import="DBWorks.DBConnection"%>
<%@page import="java.sql.SQLException"%>
<%@ page import="java.sql.ResultSet" %>

<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Bloom</title>

        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="css/sb-admin-2.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>

        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="CRHome.jsp">Bloom-Customer-Representative</a>
                </div>
                <!-- /.navbar-header -->

                <ul class="nav navbar-top-links navbar-right">
                    <!-- /.dropdown -->
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="CRHelp.jsp"><i class="fa fa-question fa-fw"></i> Help</a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="logout.jsp"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                            </li>
                        </ul>
                        <!-- /.dropdown-user -->
                    </li>
                    <!-- /.dropdown -->
                </ul>
                <!-- /.navbar-top-links -->

                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <li>
                        <a href="CreateAd.jsp"><i class="fa fa-file-text-o fa-fw"></i> Create an advertisement</a>
                    </li>
                    <li>
                        <a href="DeleteAd.jsp"><i class="fa fa-trash-o fa-fw"></i> Delete an advertisement</a>
                    </li>
                    <li>
                        <a href="ViewTransaction.jsp"><i class="fa fa-fw fa-pencil"></i> Record a transaction</a>
                    </li>
                    <li>
                        <a href="EditCustomerInfo.jsp"><i class="fa fa-edit fa-fw"></i> Edit customer information</a>
                    </li>
                    <li>
                        <a href="MailingList.jsp"><i class="fa fa-fw fa-list"></i> Customer mailing list</a>
                    </li>
                    <li>
                        <a href="SuggestList.jsp"><i class="fa fa-fw fa-check-square-o"></i> Item suggestions list</a>
                    </li>
                            

                        </ul>
                    </div>
                    <!-- /.sidebar-collapse -->
                </div>
                <!-- /.navbar-static-side -->
            </nav>

            <!-- Page Content -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Edit customer information</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                
                <%
                    int customerid = Integer.parseInt(request.getParameter("customerid"));
                    String query = "SELECT * FROM customer WHERE CustomerId = '"
                            + customerid + "'";
                    java.sql.ResultSet rs = DBConnection.ExecQuery(query);
                    if (rs.next()) {
                %>      
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-danger">
                            <div class="panel-heading">
                                <i class="fa fa-edit fa-fw"></i> Edit customer information
                            </div>
                            <div class="panel-body">
                                <form role="form" method="get" action="newInfo.jsp">
                                    <div class="row">
                                        <div class="col-lg-6">

                                            <input type="hidden" name="customerid" value="<%= customerid%>" >
                                            <div class="form-group">
                                                <p> <strong>Customer ID: </strong> <%= customerid%></p>                                                 
                                            </div>
                                            <div class="form-group">
                                                <lable>Last Name *</lable>
                                                <input type="text" class="form-control" name="newlastname" value="<%= rs.getString("LastName")%>">
                                            </div>
                                            <div class="form-group">
                                                <lable>First Name *</lable>
                                                <input type="text" class="form-control" name="newfirstname" value="<%= rs.getString("FirstName")%>">                                                   
                                            </div>
                                            <div class="form-group">
                                                <lable>Address</lable>
                                                <input class="form-control" name="newaddress" value="<%= rs.getString("Address")%>">                                                 
                                            </div> 
                                            <div class="form-group">
                                                <label>City</label>
                                                <input type="text" class="form-control" name="newcity" value="<%= rs.getString("City")%>">
                                            </div>
                                            <div class="form-group">
                                                <label>State</label>
                                                <input type="text" class="form-control" name="newstate" value="<%= rs.getString("State")%>">
                                            </div>
                                            <div class="form-group">
                                                <label>Zip Code</label>
                                                <input type="text" class="form-control" name="newzip" value="<%= rs.getString("ZipCode")%>">                                                
                                            </div>
                                            <!-- </form>-->
                                        </div>
                                        <!-- /.col-lg-6 (nested) -->
                                        <div class="col-lg-6">
                                            <!--<form role="form">-->
                                            <div class="form-group">
                                                <label>Email *</label>
                                                <input type="text" class="form-control" name="newemail" value="<%= rs.getString("Email")%>">
                                            </div>
                                            <div class="form-group">
                                                <label>Date of Birth (yyyy/MM/dd)</label>
                                                <input type="text" class="form-control" name="newdob" value="<%= rs.getString("DOB")%>">
                                            </div>
                                            <div class="form-group">
                                                <label>Sex (F/M)</label>
                                                <input type="tel" class="form-control" name="newsex" value="<%= rs.getString("Sex")%>">
                                            </div> 
                                            <div class="form-group">
                                                <label>Telephone</label>
                                                <input type="tel" class="form-control" name="newtel" value="<%= rs.getString("Telephone")%>">
                                            </div>
                                            <div class="form-group">
                                                <label>Creation Date</label>  
                                                <p> <%= rs.getString("CreationDate")%></p>
                                            </div>

                                            <div class="form-group">
                                                <label>Rating</label>
                                                <input type="text" class="form-control" name="newrating" value="<%= rs.getString("Rating")%>">
                                            </div>                                           
                                        </div>
                                        <!-- /.col-lg-6 (nested) -->
                                    </div>
                                    <!-- /.row (nested) -->
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <!--<form role="form">-->
                                            <button type="submit" class="btn btn-outline btn-primary" vlaue="add">Submit</button>
                                            <button type="reset" class="btn btn-outline btn-danger">Reset</button>
                                            <!-- </form>-->
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
                <% } %>
            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->

        <!-- jQuery Version 1.11.0 -->
        <script src="js/jquery-1.11.0.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="js/plugins/metisMenu/metisMenu.min.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="js/sb-admin-2.js"></script>

    </body>

</html>