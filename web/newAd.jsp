<%-- 
    Document   : newAd
    Created on : Apr 29, 2015, 3:02:06 PM
    Author     : jodiekwok
--%>

<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%@page import="DBWorks.DBConnection"%>
<%@page import="java.sql.SQLException"%>
<%@ page import="java.sql.ResultSet" %>

<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Bloom</title>

        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="css/sb-admin-2.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>

        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="CRHome.jsp">Bloom-Customer-Representative</a>
                </div>
                <!-- /.navbar-header -->

                <ul class="nav navbar-top-links navbar-right">
                    <!-- /.dropdown -->
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="CRHelp.jsp"><i class="fa fa-question fa-fw"></i> Help</a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="logout.jsp"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                            </li>
                        </ul>
                        <!-- /.dropdown-user -->
                    </li>
                    <!-- /.dropdown -->
                </ul>
                <!-- /.navbar-top-links -->

                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">                           
                            <li>
                        <a href="CreateAd.jsp"><i class="fa fa-file-text-o fa-fw"></i> Create an advertisement</a>
                    </li>
                    <li>
                        <a href="DeleteAd.jsp"><i class="fa fa-trash-o fa-fw"></i> Delete an advertisement</a>
                    </li>
                    <li>
                        <a href="ViewTransaction.jsp"><i class="fa fa-fw fa-pencil"></i> Record a transaction</a>
                    </li>
                    <li>
                        <a href="EditCustomerInfo.jsp"><i class="fa fa-edit fa-fw"></i> Edit customer information</a>
                    </li>
                    <li>
                        <a href="MailingList.jsp"><i class="fa fa-fw fa-list"></i> Customer mailing list</a>
                    </li>
                    <li>
                        <a href="SuggestList.jsp"><i class="fa fa-fw fa-check-square-o"></i> Item suggestions list</a>
                    </li>
                            

                        </ul>
                    </div>
                    <!-- /.sidebar-collapse -->
                </div>
                <!-- /.navbar-static-side -->
            </nav>

            <!-- Page Content -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Create Advertisement</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <%
                    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
                    Date date = new Date();
                    String advdate = dateFormat.format(date);
                    String type = request.getParameter("type");
                    String name = request.getParameter("name");
                    String content = request.getParameter("content");
                    String companyname = request.getParameter("companyname");
                    String getunits = request.getParameter("availableunits");
                    float unitprice = 0;
                    if (type.equals("") || name.equals("") || content.equals("")
                            || companyname.equals("") || getunits.equals("")) {
                %>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-danger">
                            <div class="panel-heading">Please enter all the necessary information!</div>
                        </div>
                    </div>
                </div>
                <%
                } else {
                    if (!request.getParameter("unitprice").equals("")) {
                        unitprice = Float.parseFloat(request.getParameter("unitprice"));
                    }
                    int availableunits = Integer.parseInt(getunits);
                    Integer employeeid = (Integer) session.getAttribute("ID");
                    String advid = "SELECT MAX(AdvertisementId) FROM advertisement";
                    int newadvid = 0;
                    java.sql.ResultSet rsadvid = DBConnection.ExecQuery(advid);
                    if (rsadvid.next()) {
                        newadvid = rsadvid.getInt("MAX(AdvertisementId)") + 1;
                        }
                    String query = "INSERT INTO advertisement VALUES (" + newadvid +
                            "," + employeeid + ",'" + type + "','" + advdate +
                            "','" + companyname + "','" + name + "'," + availableunits +
                            "," + unitprice + ",'" + content + "')";
                    DBConnection.ExecUpdateQuery(query);
                %>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-success">
                            <div class="panel-heading">Advertisement created successfully!</div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <i class="fa fa-file-text-o fa-fw"></i>
                                New Advertisement Information
                            </div>
                            <div class="panel-body">
                                
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <!--<form role="form">-->
                                            <p> <strong>Advertisement ID: </strong><%= newadvid%> </p>
                                            <p> <strong>Item Name: </strong> <%= name%></p>
                                            <p> <strong>Type: </strong> <%= type%></p>
                                            <p> <strong>Advertisement Contents: </strong> </p>
                                            <p> <%= content%></p>
                                                                                 
                                            <!-- </form>-->
                                        </div>
                                        <!-- /.col-lg-6 (nested) -->
                                        <div class="col-lg-6">
                                            <!--<form role="form">-->
                                            <p> <strong>Date: </strong> <%= advdate%></p>
                                            <p> <strong>Company Name: </strong> <%= companyname%> </p>
                                            <p> <strong>Available Units: </strong> <%= availableunits%></p>
                                            <p> <strong>Unit price: </strong> <%= unitprice%></p>
                                            
                                            <!-- </form>-->
                                        </div>
                                        <!-- /.col-lg-6 (nested) -->
                                    </div>
                                    <!-- /.row (nested) -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <% }%>
            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->

        <!-- jQuery Version 1.11.0 -->
        <script src="js/jquery-1.11.0.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="js/plugins/metisMenu/metisMenu.min.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="js/sb-admin-2.js"></script>

    </body>

</html>
